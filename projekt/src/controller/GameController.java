package controller;

import model.board.*;
import model.game.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JFrame;

import view.BoardGui;
import view.OthelloGui;

public class GameController {
	private Game game = null;
	private String savePath = "../saves.txt";
	
	private SaveGame saves;
	private int freeze = 0;
	private int level = 0;
	private int mode = 0;
	private BufferedReader bf;
	
	private OthelloGui frame;
	
	public GameController(OthelloGui frame) {
		this.frame = frame;
		String content = getContent();
		this.saves = new SaveGame(content);
	}
	
	public Game getGame() {
		return (game);
	}

	public int getModeGame() {
		return (mode);
	}
	
	/**
	 * Vytvoří nový frame a a hrací pole
	 * 
	 * @param size Velikost hrací desky
	 * @param mode Hráč proti hráči nebo počítači
	 * @param level Úroveň obtížnosti počítače
	 * @param freeze Nastavení zamrzání
	 */
	public void mainMenuStart(int size, int mode, int level, int freeze) {
		GameController game = new GameController(frame);
		game.startNewGame(size, mode, level, freeze);
		BoardGui gui = new BoardGui(game);
		gui.setVisible(true);
	}
	
	
	/**
	 * Inicializuje hru
	 * 
	 * @param size Velikost hrací desky
	 * @param mode Hráč proti hráči nebo počítači
	 * @param level Úroveň obtížnosti počítače
	 * @param freeze Nastavení zamrzání
	 */
	public void startNewGame(int size, int mode, int level, int freeze) {
		this.mode = mode;
		this.level = level;
		this.freeze = freeze;

		ReversiRules rules = new ReversiRules(size);
		Board board = new Board(rules);
		this.game = new Game(board);

		game.addPlayer(new Player(true));
		game.addPlayer(new Player(false));
	}

	/**
	 * Funkce vrací hrací desku
	 * 
	 * @return Board
	 */
	public Board getBoard() {
		return (this.game.getBoard());
	}

	/**
	 * Funkce ověří, zda aktualní hráč může na políčko vložit kámen
	 * 
	 * @param row Řádek
	 * @param col Sloupec
	 * @return boolean Úspěch vložení
	 */
	public boolean canPutDisk(int row, int col) {
		return (game.currentPlayer().canPutDisk(getBoard().getField(row, col)));
	}

	/**
	 * Vraci číslo hráče 0 - Player1 1 - Player2 / Computer
	 * 
	 * @return Číslo hráče
	 */
	public int onMove() {
		return ((this.game.currentPlayer().isWhite()) ? 0 : 1);
	}

	/**
	 * Změní aktuálního hráče
	 */
	public void nextPlayer() {
		game.nextPlayer();
	}

	/**
	 * Vrátí druhého hráče
	 * 
	 * @return Druhý hráč
	 */
	public Player otherPlayer() {
		game.nextPlayer();
		Player p = game.currentPlayer();
		game.nextPlayer();
		return (p);
	}

	/**
	 * Provede operaci UNDO
	 */
	public void undo() {
		if (this.mode == 1)
			game.undo();
		game.undo();
	}

	/**
	 * Hráč položí disk na dané souřadnice
	 * 
	 * @param row Řádek
	 * @param col Sloupec
	 * @return Úspěch operace
	 */
	public Field playerPushDisk(int row, int col) {
		Field field = null;
		if ((row > 0) && (col > 0) && (row <= game.getBoard().getSize())
				&& (col <= game.getBoard().getSize())) {
			if ((mode == 0) || (onMove() == 0)) {
				field = getBoard().getField(row, col);
				if (this.game.currentPlayer().putDisk(field)) {
					this.game.nextPlayer();
				} else
					field = null;
			}
		}
		return (field);
	}

	/**
	 * Počítač položí káman na nalezené pole
	 * 
	 * @return Úspěch operace
	 */
	public Field pcPushDisk() {
		Field field = getFieldPc();
		if (field != null) {
			game.currentPlayer().putDisk(field);
		}
		game.nextPlayer();
		return (field);
	}

	private Field getFieldPc() {
		if (level == 1) return(getTheBestField());
		else return (this.getFirstField());
	}

	/**
	 * Najde první možné políčko kam je možné vložit kámen
	 * 
	 * @return Možné pole
	 */
	private Field getFirstField() {
		for (int row = 1; row <= getBoard().getSize(); row++) {
			for (int col = 1; col <= getBoard().getSize(); col++) {
				if (canPutDisk(row, col)) {
					return (getBoard().getField(row, col));
				}
			}
		}
		return (null);
	}
	
	/**
	 * Najde pole, na které se vloží kámen a druhý hráč bude mít minimum možností kam vložit kámen
	 * 
	 * @return Možné pole
	 */
	private Field getTheBestField() {
		Field field = null;
		int canPut = 144;
		for (int row = 1; row <= game.getBoard().getSize(); row++) {
			for (int col = 1; col <= game.getBoard().getSize(); col++) {
				if (game.currentPlayer().putDisk(game.getBoard().getField(row, col))) {
					game.nextPlayer();
					int sCanPut = 0;
					for (int sRow = 0; sRow < game.getBoard().getSize(); sRow++) {
						for (int sCol = 0; sCol < game.getBoard().getSize(); sCol++) {
							if (game.currentPlayer().canPutDisk(game.getBoard().getField(sRow, sCol)))
								sCanPut++;
						}
					}
					if (sCanPut < canPut) {
						canPut = sCanPut;
						field = game.getBoard().getField(row, col);
					}
					game.undo();
				}
			}
		}
		return (field);
	}

	private String getContent() {
		File file = new File(savePath);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e1) {
				System.err.println("Soubor (" + savePath + ") nelze vytvořit.");
			} 
			
		}
		String result = "";
		try {
			bf = new BufferedReader(new FileReader(savePath));
			String line = bf.readLine();
			while (line != null) {
				result += line;
				line = bf.readLine();
			}
		} catch (FileNotFoundException e) {
			System.err.println("Soubor (" + savePath + ") nebyl nalezen.");
		} catch (IOException e) {
			System.err.println("Soubor (" + savePath + ") nelze přečíst.");
		} finally {
			try {
				bf.close();
			} catch (IOException e) {
				System.err.println("Pokus o zavření soubru selhal.");
			}
		}
		return (result);
		
	}

	/**
	 * Uloží hru
	 * 
	 * @return Úspěch operace
	 */
	public boolean saveGame(String name) {
		boolean result = saves.addNewSave(name, mode, level, onMove(),
				freeze, game.getBoard());
		Path file = Paths.get(savePath);
		try {
			Files.write(file, saves.getDataToSave(), Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return (result);
	}
	
	/**
	 * Zkotroluje da je prázdný slot pro uložení
	 * 
	 * @return Výsledek operace
	 */
	public boolean isEmptySaveSlot() {
		return (!saves.isFull());
	}
	
	/**
	 * Získá počet uložených her
	 * 
	 * @return Počet uložených her
	 */
	public int getCoutSaves() {
		return (saves.getCountSaves());
	}
	
	/**
	 * Získá maximální počet uložených her
	 * 
	 * @return Počet
	 */
	public int getMaxCountSaves() {
		return (saves.getMaxCountSaves());
	}

	/**
	 * Získá jméno uložené hry
	 * 
	 * @param numOfSave Číslo uložené hry
	 * @return Název
	 */
	public String getNameOfSave(int numOfSave) {
		return (saves.getNameOfSave(numOfSave));
	}
	
	public void mainMenuLoad(int save){
		GameController game = new GameController(frame);
		game.loadGame(save);
		BoardGui gui = new BoardGui(game);
		gui.setVisible(true);
	}
	
	
	/**
	 * Načte již uloženou hru
	 * 
	 * @param numOfSave Číslo uložené hry
	 */
	public void loadGame(int numOfSave) {
		this.mode = saves.getModeOfSave(numOfSave);
		this.level = saves.getLevelOfSave(numOfSave);
		this.freeze = saves.getOnFreezeOfSave(numOfSave);

		ReversiRules rules = new ReversiRules(saves.getSizeOfSave(numOfSave));
		Board board = new Board(rules);
		this.game = new Game(board);

		Player p0 = new Player(true);
		Player p1 = new Player(false);
		game.addPlayer(p0);
		game.addPlayer(p1);
		
		if (onMove() != saves.getOnMoveOfSave(numOfSave))
			nextPlayer();
		p0.addCountDisk();
		p0.addCountDisk();
		p1.addCountDisk();
		p1.addCountDisk();
		
		for (int i = 1; i <= saves.getSizeOfSave(numOfSave); i++) {
			for (int j = 1; j <= saves.getSizeOfSave(numOfSave); j++) {
				if (saves.getBoardSave(numOfSave).get(i-1).get(j-1) == 0) {
					if (game.getBoard().getField(i, j).putDisk(new Disk(true)) == false) {
						if (game.getBoard().getField(i, j).getDisk().isWhite() != true) 
							game.getBoard().getField(i, j).getDisk().turn();
					}
					p0.removeCountDisk();
				} else if (saves.getBoardSave(numOfSave).get(i-1).get(j-1) == 1) {
					game.getBoard().getField(i, j).putDisk(new Disk(false));
					if (game.getBoard().getField(i, j).putDisk(new Disk(true)) == false) {
						if (game.getBoard().getField(i, j).getDisk().isWhite() != false) 
							game.getBoard().getField(i, j).getDisk().turn();
					}
					p1.removeCountDisk();
				}
			}
		}
		
		saves.destroySave(numOfSave);
		
		Path file = Paths.get(savePath);
		try {
			Files.write(file, saves.getDataToSave(), Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
	}
}
