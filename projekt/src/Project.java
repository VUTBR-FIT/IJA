import view.OthelloGui;
public class Project {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				OthelloGui frame = new OthelloGui();
				frame.showMainMenu();
				frame.setVisible(true);
			}
		});
	}

}
