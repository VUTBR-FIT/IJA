package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.board.Board;
import model.board.Disk;
import model.board.Field;
import model.game.Player;

import controller.GameController;

import view.BoardGui.Actions;

public class BoardPanel extends JPanel {

	private int sizeField = 75;
	private int borderField = 5;
	private int border = 5;
	private int size;
	private int mode;
	private int possibleMoves;
	private int possibleMoves2;
	private int pushedDisk;
	private int pushedDisk2;
	private GameController controller;
	private BoardGui gui;

	public BoardPanel(GameController controller, int size, int mode,
			BoardGui gui) {
		this.controller = controller;
		this.size = size;
		this.gui = gui;
		this.mode = mode;
		this.setPreferredSize(new Dimension(getWidth(), getHeight()));
		this.setBackground(Color.YELLOW);
		this.setLayout(null);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		this.possibleMoves = 0;
		this.possibleMoves2 = 0;
		this.pushedDisk = 0;
		this.pushedDisk2 = 0;

		Player otherPlayer = controller.otherPlayer();

		Graphics2D g2 = (Graphics2D) g;
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, getWidthBoard(), getHeight());
		panel.setPreferredSize(new Dimension(getWidthBoard(), getHeight()));
		panel.setBackground(Color.BLUE);
		panel.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				actionCLicked(arg0.getX(), arg0.getY());
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
			}
		});

		Board board = controller.getBoard();
		Disk disk;

		// Pozadi pro hraci desku
		new BoardRecTangle(0, 0, getWidthBoard(), getHeight()).draw(g2,
				Color.BLUE);
		// Pozadí pro postraního panel
		// new BoardRecTangle(getWidthBoard(), 0, getWidthRightMenu(),
		// getHeight()).draw(g2, Color.RED);

		this.possibleMoves = 0;

		for (int row = 0; row < size; row++) {
			for (int col = 0; col < size; col++) {
				if ((disk = board.getField(row + 1, col + 1).getDisk()) == null) { // policko
																					// je
																					// prazdne
					if (controller.canPutDisk(row + 1, col + 1)) {
						if ((this.mode != 1) || (controller.onMove() != 1)) {
							new BoardRecTangle(col * (sizeField + borderField),
									row * (sizeField + borderField), sizeField,
									sizeField).draw(g2, Color.GREEN);

						} else {
							new BoardRecTangle(col * (sizeField + borderField),
									row * (sizeField + borderField), sizeField,
									sizeField).draw(g2, Color.GRAY);
						}
						this.possibleMoves++;
					} else {
						new BoardRecTangle(col * (sizeField + borderField), row
								* (sizeField + borderField), sizeField,
								sizeField).draw(g2, Color.GRAY);
					}
					if (otherPlayer.canPutDisk(controller.getBoard().getField(
							row, col)))
						this.possibleMoves2++;
				} else { // policko neco obsahuje
					if (disk.isWhite()) {
						new BoardRecTangle(col * (sizeField + borderField), row
								* (sizeField + borderField), sizeField,
								sizeField).draw(g2, Color.WHITE);
						if (controller.getGame().currentPlayer().isWhite())
							this.pushedDisk++;
						else
							this.pushedDisk2++;
					} else {
						new BoardRecTangle(col * (sizeField + borderField), row
								* (sizeField + borderField), sizeField,
								sizeField).draw(g2, Color.BLACK);
						if (!controller.getGame().currentPlayer().isWhite())
							this.pushedDisk++;
						else
							this.pushedDisk2++;
					}
				}
			}
		}
		this.add(panel);
	}

	private void checkDisk() {
		possibleMoves = 0;
		possibleMoves2 = 0;

		Player otherPlayer = controller.otherPlayer();
		
		for (int i = 1; i <= controller.getBoard().getSize(); i++) {
			for (int j = 1; j <= controller.getBoard().getSize(); j++) {
				if (controller.canPutDisk(i, j))
					possibleMoves++;
				else if (otherPlayer.canPutDisk(controller.getBoard().getField(i, j)))
					possibleMoves2++;
			}
		}
	}
	
	public synchronized void checkSpecialStates() {
		checkDisk();
		
		if (possibleMoves == 0) {
			if (possibleMoves2 == 0) {
				new WaiterGui(this, 1, Actions.EndGame).start();
				//konec hry
			} else {
				new WaiterGui(this, 1, Actions.NoMoves).start();
				//zmena hrace
			}
		} else {
			if (controller.getModeGame() == 1) {
				new WaiterGui(this, 1, Actions.NextPlayer).start();
				// jsou dostupen tahy ale na tahu je pocitac
			}
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		Graphics2D g2 = (Graphics2D) g;
		JButton back = new JButton("Exit");
		back.setPreferredSize(new Dimension(150, 40));
		back.setBounds(getWidthBoard() + 25, 10, 150, 40);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				checkAction(Actions.Exit);
			}
		});

		this.add(back);

		JButton undo = new JButton("UNDO");
		undo.setPreferredSize(new Dimension(150, 40));
		undo.setBounds(getWidthBoard() + 25, 60, 150, 40);
		undo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				checkAction(Actions.Undo);
			}
		});
		this.add(undo);

		JButton save = new JButton("Save");
		save.setPreferredSize(new Dimension(150, 40));
		save.setBounds(getWidthBoard() + 25, 110, 150, 40);
		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				checkAction(Actions.Save);
			}
		});
		this.add(save);

		new BoardRecTangle(getWidthBoard(), 160, getWidthRightMenu(), 150)
				.draw(g2, Color.WHITE);

		JPanel pWhite = new JPanel();
		pWhite.setBounds(getWidthBoard(), 160, getWidthRightMenu(), 150);
		pWhite.setPreferredSize(new Dimension(getWidthRightMenu(), 150));
		pWhite.setBackground((controller.onMove() == 0) ? Color.GREEN
				: Color.WHITE);
		JPanel pSubWhite = new JPanel();
		pSubWhite.setPreferredSize(new Dimension(getWidthRightMenu() - 30,
				150 - 30));
		JLabel white = new JLabel("Player WHITE");
		white.setPreferredSize(new Dimension(getWidthRightMenu() - 40, 100));
		pSubWhite.add(white);
		pWhite.add(pSubWhite);

		JPanel pBlack = new JPanel();
		pBlack.setBounds(getWidthBoard(), 310, getWidthRightMenu(), 150);
		pBlack.setPreferredSize(new Dimension(getWidthRightMenu(), 150));
		pBlack.setBackground((controller.onMove() == 1) ? Color.GREEN
				: Color.WHITE);
		JPanel pSubBlack = new JPanel();
		pSubBlack.setPreferredSize(new Dimension(getWidthRightMenu() - 30,
				150 - 30));
		JLabel black = new JLabel();
		black.setText("Player BLACK");

		black.setPreferredSize(new Dimension(getWidthRightMenu() - 40, 100));
		pSubBlack.add(black);
		pBlack.add(pSubBlack);
		
		JLabel naRade = new JLabel();
		naRade.setText("Na řadě je: " + controller.getGame().currentPlayer());
		naRade.setPreferredSize(new Dimension(150, 40));
		naRade.setBounds(getWidthBoard() + 25, 150, 150, 40);
		this.add(naRade);
		
		Disk disk;
		pushedDisk = 0;
		pushedDisk2 = 0;
		for (int i = 1; i <= controller.getBoard().getSize(); i++) {
			for (int j = 1; j <= controller.getBoard().getSize(); j++) {
				if ((disk= controller.getBoard().getField(i,j).getDisk()) != null) {
					if (disk.isWhite()) {
						pushedDisk++;
					} else pushedDisk2++;
				}
			}
		}
		
		
		JLabel skore = new JLabel();
		skore.setText("Hráč B SCORE: " + pushedDisk);
		skore.setPreferredSize(new Dimension(150, 40));
		skore.setBounds(getWidthBoard() + 25, 190, 150, 40);
		this.add(skore);
		JLabel skore2 = new JLabel();
		skore2.setText("Hráč Č SCORE: " + pushedDisk2);
		skore2.setPreferredSize(new Dimension(150, 40));
		skore2.setBounds(getWidthBoard() + 25, 230, 150, 40);
		
		
		JLabel skoreP = new JLabel();
		skoreP.setText("Dostupné disky: " + ((controller.getBoard().getSize() * controller.getBoard().getSize()) - pushedDisk - pushedDisk2));
		skoreP.setPreferredSize(new Dimension(160, 40));
		skoreP.setBounds(getWidthBoard() + 25, 210, 160, 40);
		this.add(skoreP);
		this.add(skore2);
		JLabel skoreP2 = new JLabel();
		skoreP2.setText("Dostupné disky: " + ((controller.getBoard().getSize() * controller.getBoard().getSize()) - pushedDisk - pushedDisk2));
		skoreP2.setPreferredSize(new Dimension(160, 40));
		skoreP2.setBounds(getWidthBoard() + 25, 250, 160, 40);
		this.add(skoreP2);
	}

	/**
	 * Provede náležité operace na základě předané akce
	 * 
	 * @param action
	 *            Akce
	 */
	public synchronized void checkAction(Actions action) {
		if (action == Actions.Wait)
			return;
		if (action == Actions.NoMoves) {
			System.out.println(((controller.onMove() == 1)? "Player 2" : "Player 1") + " -> NO POSSIBLE MOVES");
			JOptionPane.showMessageDialog (null, "Nemáte žádné další tahy", "Konec hry", JOptionPane.INFORMATION_MESSAGE);
			controller.nextPlayer();
			checkSpecialStates();
		} else if (action == Actions.Undo) {
			System.out.println(((controller.onMove() == 1)? "Player 2" : "Player 1") + " -> CALL UNDO");
			controller.undo();
		} else if (action == Actions.NextPlayer) {
			if ((controller.getModeGame() == 1) && (controller.onMove() == 1)) {
				Field field = controller.pcPushDisk();
				if (field != null)
					System.out.println("PC -> " + field.toString());
				checkSpecialStates();
			} else
				return;
		} else if (action == Actions.Save) {
			if ((controller.getModeGame() != 1) || (controller.onMove() != 1)) {
				if (controller.isEmptySaveSlot()) {
					
					String name = JOptionPane.showInputDialog(new JFrame(),
							"Zadejte jméno, pod kterým se bude hra uložena:",
							"NEW SAVE");
					if (name != null) {
						controller.saveGame(name);
						setVisible(false);
						gui.dispose();
					}
				} else {
					JOptionPane.showMessageDialog(new JFrame(),
							"Nelze uložit hru, paměť je plná");
				}
			}
			return;
		} else if (action == Actions.MainMenu) {

			return;
		} else if (action == Actions.Exit) {
			setVisible(false);
			gui.dispose();
			return;
		} else if (action == Actions.Help) {
			/*JOptionPane
					.showMessageDialog(
							this,
							"Vitajte v hre Othello! \nPravidla hry viz.: https://cs.wikipedia.org/wiki/Othello_%28deskov%C3%A1_hra%29#Pravidla_hry \nAutori:\nxcerny63\nxblask02");
			*/return;
		} else if (action == Actions.EndGame) {
			String data;
			checkDisk();
			if (pushedDisk > pushedDisk2) data = "Vyhravá černý hráč (" + pushedDisk +")";
			else data = "Vyhravá bílý hráč (" + pushedDisk2 +")";
				
			System.out.println(data);
			
			
			JOptionPane.showMessageDialog (null, data, "Konec hry", JOptionPane.INFORMATION_MESSAGE);
			gui.setVisible(false);
			gui.dispose();
			return;
		}
		this.removeAll();
		this.revalidate();
		this.repaint();
	}

	public int getWidthRightMenu() {
		return (200);
	}

	public int getWidthBoard() {
		return (size * (sizeField + borderField));
	}

	public int getWidth() {
		return (getWidthBoard() + getWidthRightMenu() + 2 * border);
	}

	public int getHeight() {
		return (size * (sizeField + borderField) - borderField + 2 * border);
	}

	public void actionCLicked(int x, int y) {
		int row = (int) Math.floor(y / (sizeField + borderField)) + 1;
		int col = (int) Math.floor(x / (sizeField + borderField)) + 1;
		if ((mode == 1) && (controller.onMove() == 1))
			return;
		if ((x % (sizeField + borderField) < sizeField)
				&& (y % (sizeField + borderField) < sizeField)) {
			Field field = this.controller.playerPushDisk(row, col);
			if (field != null) {
				System.out.println("Player " + field.toString());
				this.removeAll();
				this.revalidate();
				this.repaint();
			}
			checkSpecialStates();
		}
	}
	
	class BoardRecTangle {
		private int possitionX;
		private int possitionY;
		private int width;
		private int height;

		public BoardRecTangle(int x, int y, int width, int height) {
			this.possitionX = x;
			this.possitionY = y;
			this.width = width;
			this.height = height;
		}

		public void draw(Graphics2D g, Color color) {
			g.setColor(color);
			g.fillRect(this.possitionX, this.possitionY, this.width,
					this.height);
		}

	}
}
