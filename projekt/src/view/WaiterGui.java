package view;

import view.BoardGui.Actions;

class WaiterGui extends Thread {
	private BoardPanel view;
	private int seconds = 1;
	private Actions action;
	
	public WaiterGui (BoardPanel view, int sec, Actions action) {
		this.view = view;
		this.seconds = sec;
		this.action = action;
	}
	
	public void run() {
		try {
			Thread.sleep(1000 * seconds);
		} catch (InterruptedException e) {
			System.err.println("Waiter FAILED in sleep(1000 * seconds)");
		}
		view.checkAction(action);
	}
}
