package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.GameController;

/**
 * Třída reprezentující herní okno
 * 
 * @author Lukáš Černý (xcerny63)
 * @author Adriana Blašková (xblask02)
 *
 */
public class BoardGui extends JFrame {
    private GameController controller = null;
   
    public enum Actions
    {
        NoMoves,
        Undo,
        ClickPlayer,
        ClickComputer,
        NextPlayer,
        MainMenu,
        EndGame,
        Exit,
        Save,
        Help,
        Wait
    }
    
    /**
     * Inicializace
     * 
     * @param controller Instace kontroleru
     */
    public BoardGui(GameController controller) {
    	this.controller = controller;
		setTitle("Othello - GAME");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		BoardPanel panel = new BoardPanel(controller, controller.getBoard().getSize(), controller.getModeGame(), this);
		addPanel(panel);
		showView(panel.getWidth(), panel.getHeight());
		
		setBackground(new Color(140, 245, 12));
		setForeground(new Color(161, 249, 21));
		
		setVisible(true);
	}

    /**
     * Přidá panel
     * 
     * @param panel Přidávaný panel
     */
	public void addPanel(JPanel panel) {
		this.add(panel);
		this.pack();
	}

	/**
	 * Vycentruje okno
	 * 
	 * @param width Šířka
	 * @param height Výška
	 */
	public void showView(int width, int height) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width / 2 - width / 2, dim.height / 2 - height / 2); // Okno bude uprostred obrazovky
	}
}
