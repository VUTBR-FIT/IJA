package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import controller.GameController;

/**
 * Třída reprezentují hlavní menu a menu pro načítání
 * 
 * @author Lukáš Černý (xcerny63)
 * @author Adriana Blašková (xblask02)
 *
 */
public class OthelloGui extends JFrame {
	private String name = "Othello";
	private GameController controller = null;
	private ButtonGroup groupNumPlayer;
	private ButtonGroup groupSizeBoard;
	private ButtonGroup groupLevel;
	private JCheckBox freeze;
	
	/**
	 * Inicializace
	 */
	public OthelloGui() {
		setTitle(this.name);
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	}

	/**
	 * Zobrazí otevírané okno ve středu obrazovky
	 * 
	 * @param Šířka
	 * @param Výška
	 */
	private void showInCenter(int width, int height) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width / 2 - width / 2, dim.height / 2 - height / 2); // Okno bude uprostred obrazovky
	}
	
	/**
	 * Zobrazí hlavní menu
	 */
	public void showMainMenu() {
		getContentPane().removeAll();
		showInCenter(400, 500);
		
		Color defColor = new Color(138, 248, 7);
		
		setBackground(new Color(140, 245, 12));
		setForeground(new Color(161, 249, 21));
		
		JPanel panel = new JPanel();
		panel.setBackground(defColor);
		panel.setBorder(javax.swing.BorderFactory.createMatteBorder(5, 5, 5, 5, new Color(249, 246,8)));
		
		// Nadpis
		JLabel title = new JLabel();
		title.setFont(new Font("Cantarell", 1, 48));
		title.setForeground(new Color(12, 11, 8));
		title.setText(name);
		
		// Vyber poctu hracu
		JLabel numPlayer = new JLabel();
		numPlayer.setFont(new Font("Cantarell", 1, 15));
		numPlayer.setText("Number of players");
		
		groupNumPlayer = new ButtonGroup();
		JRadioButton onePlayer = new JRadioButton();
		onePlayer.setText("1");
		onePlayer.setActionCommand("1");
		onePlayer.setSelected(true);
		onePlayer.setBackground(defColor);
		JRadioButton twoPlayer = new JRadioButton();
		twoPlayer.setText("2");
		twoPlayer.setActionCommand("2");
		twoPlayer.setBackground(defColor);
		groupNumPlayer.add(onePlayer);
		groupNumPlayer.add(twoPlayer);
		
		JButton loadButton = new JButton();
		loadButton.setText("Load game");
		loadButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				showLoadMenu();
			}
		});
		
		// Velikost hraci desky
		JLabel boardSize = new JLabel();
		boardSize.setFont(new Font("Cantarell", 1, 15));
		boardSize.setText("Board size");
		
		groupSizeBoard = new ButtonGroup();
		JRadioButton six = new JRadioButton();
		six.setText("6x6");
		six.setActionCommand("6");
		six.setBackground(defColor);
		JRadioButton eight = new JRadioButton();
		eight.setText("8x8");
		eight.setActionCommand("8");
		eight.setSelected(true);
		eight.setBackground(defColor);
		JRadioButton ten = new JRadioButton();
		ten.setText("10x10");
		ten.setActionCommand("10");
		ten.setBackground(defColor);
		JRadioButton twelve = new JRadioButton();
		twelve.setText("12x12");
		twelve.setActionCommand("12");
		twelve.setBackground(defColor);
		groupSizeBoard.add(six);
		groupSizeBoard.add(eight);
		groupSizeBoard.add(ten);
		groupSizeBoard.add(twelve);
		
		JButton start = new JButton();
		start.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				startNewGame();
			}
		});
		start.setText("Start");
		
		JButton exit = new JButton();
		exit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		exit.setText("Exit");
		
		freeze = new JCheckBox();
		freeze.setBackground(defColor);
		freeze.setText("Allow freezing disks");
		
		// Nastave obtiznosti
		JLabel level = new JLabel();
		level.setFont(new Font("Cantarell", 1, 15));
		level.setText("Set difficult");
		groupLevel = new ButtonGroup();
		JRadioButton easy = new JRadioButton();
		easy.setText("Easy");
		easy.setActionCommand("easy");
		easy.setSelected(true);
		easy.setBackground(defColor);
		JRadioButton hard = new JRadioButton();
		hard.setText("Hard");
		hard.setActionCommand("hard");
		hard.setBackground(defColor);
		groupLevel.add(easy);
		groupLevel.add(hard);
		
		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(panel);
		panel.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(exit, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(start, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(six)
                    .addComponent(boardSize)
                    .addComponent(eight)
                    .addComponent(ten)
                    .addComponent(twelve))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(hard)
                    .addComponent(easy)
                    .addComponent(level)
                    .addComponent(twoPlayer)
                    .addComponent(onePlayer)
                    .addComponent(numPlayer))
                .addGap(32, 32, 32))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(title))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(121, 121, 121)
                        .addComponent(loadButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(freeze)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(title)
                .addGap(31, 31, 31)
                .addComponent(loadButton)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(numPlayer)
                    .addComponent(boardSize))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(six)
                    .addComponent(onePlayer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(eight)
                    .addComponent(twoPlayer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ten)
                    .addComponent(level))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(twelve)
                    .addComponent(easy))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(hard)
                .addGap(27, 27, 27)
                .addComponent(freeze)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(exit)
                    .addComponent(start))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        JLabel temp = new JLabel();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(35, Short.MAX_VALUE)
                .addComponent(temp)
                .addGap(308, 308, 308))
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(temp)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
		
	}
	
	/**
	 * Zobrazí load menu
	 */
	public void showLoadMenu() {
		getContentPane().removeAll();
		
		controller = new GameController(this);
		
		showInCenter(400, 600);
		
		
		JPanel jPanel1 = new javax.swing.JPanel();
        JLabel jLabel1 = new javax.swing.JLabel();
        JButton BackMM = new javax.swing.JButton();
        
        JLabel SaveGame1 = new javax.swing.JLabel();
        JLabel SaveGame2 = new javax.swing.JLabel();
        JLabel SaveGame3 = new javax.swing.JLabel();
        JLabel SaveGame4 = new javax.swing.JLabel();
        JLabel SaveGame5 = new javax.swing.JLabel();
       

        jPanel1.setBackground(new Color(138, 248, 7));
        jPanel1.setBorder(javax.swing.BorderFactory.createMatteBorder(5, 5, 5, 5, new Color(249, 246, 8)));

        jLabel1.setFont(new java.awt.Font("Cantarell", 1, 36));
        jLabel1.setText("Saved Games");

        for (int numOfSave = 0; numOfSave < controller.getMaxCountSaves(); numOfSave++) {
        	JLabel save;
        	switch (numOfSave) {
			case 0:
				save = SaveGame1;
				break;
			case 1:
				save = SaveGame2;
				break;
			case 2:
				save = SaveGame3;
				break;
			case 3:
				save = SaveGame4;
				break;
			default:
				save = SaveGame5;
				break;
			}
        	
			String name = controller.getNameOfSave(
					numOfSave);
			if (name != null) {
				save.setText(name);
				save.addMouseListener(new tempMouseListener(numOfSave, this));
			} else {
				save.setText("- empty slot " + (numOfSave+1) + " -");
			}
		}
 
        BackMM.setText("Main Menu");
        BackMM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showMainMenu();
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(jLabel1))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addComponent(SaveGame1, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addComponent(SaveGame2, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addComponent(SaveGame3, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addComponent(SaveGame4, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addComponent(SaveGame5, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 55, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(BackMM, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(SaveGame1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(SaveGame2, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(SaveGame3, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(SaveGame4, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(SaveGame5, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
                .addComponent(BackMM)
                .addGap(22, 22, 22))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
	}
	
	/**
	 * Spustí novou hru
	 */
	private void startNewGame() {
		if (controller == null)
			controller = new GameController(this);
		controller.mainMenuStart(getBoardSize(), getGameMode(), getLevelMode(), getFreeze());
	}
	
	/**
	 * Načte a spustí uloženou hru. Následně zobrazí hlavní menu
	 * 
	 * @param numSave Číslo uložené hry
	 */
	public void loadGame(int numSave) {
		controller.mainMenuLoad(numSave);
		showMainMenu();
	}
	
	/**
	 * Získá hodnotu zaškrklého radioButtonu ze skupiny pro velikost hrací desky
	 * 
	 * @return Velikost hrací desky
	 */
	public int getBoardSize() {
		System.out.println(groupSizeBoard.getSelection().getActionCommand());
		String value = groupSizeBoard.getSelection().getActionCommand();
		int result = 8;
		if (value == "6")
			result = 6;
		else if (value == "12")
			result = 12;
		else if (value == "10")
			result = 10;
		else
			result = 8;
		return (result);
	}

	/**
	 * Získá hodnotu zaškrklého radioButtonu ze skupiny pro počet hráčů
	 * 
	 * @return Počet hráčů
	 */
	public int getGameMode() {
		String value = groupNumPlayer.getSelection().getActionCommand();
		return ((value == "2") ? 0 : 1);
	}
	
	/**
	 * Získá hodnotu zaškrklého radioButtonu ze skupiny pro obtížnost počítače
	 * 
	 * @return Obtížnost
	 */
	public int getLevelMode() {
		String value = groupLevel.getSelection().getActionCommand();
		return ((value == "easy") ? 0 : 1);
	}
	
	/**
	 * Získá hodnotu zaškrklého checkBoxu zda je povoleno zamrzání kamenů
	 * 
	 * @return Zamrzání kamenů
	 */
	public int getFreeze() {
		boolean value = freeze.isSelected();
		return ((!value) ? 0 : 1);
	}
	
	/**
	 * Třída pro obsluhu myši
	 * 
	 * @author Lukáš Černý
	 *
	 */
	class tempMouseListener implements MouseListener {

		private int numOfSave;
		private OthelloGui view;

		public tempMouseListener(int numOfSave, OthelloGui view) {
			this.numOfSave = numOfSave;
			this.view = view;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			view.loadGame(numOfSave);
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}

	}
	

}
