/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.game;

import model.board.Board;

/**
 * Třída reprezuntující hru
 * 
 * @author Lukáš Černý (xcerny63)
 * @author Adriana Blašková (xblask02)
 * 
 */
public class Game extends java.lang.Object
{

    private final Board board;
    private Player firstPlayer = null;
    private Player secondPlayer = null;
    
    private boolean currentPlayer = true; // true - firstPlayer
    private Undo actionUndo = null;

    public Game(Board board)
    {
        this.board = board;
        this.actionUndo = new Undo();
    }

    /**
     * Pokud je to mozne, tak prida hrace do hry
     * 
     * @param player Přidáváný hráč
     * @return boolean Uspech vlozeni
     */
    public boolean addPlayer(Player player)
    {
        if (this.firstPlayer != null) {
            if (firstPlayer.isWhite() == player.isWhite()) {
                return (false);
            }
        } else if (this.secondPlayer != null) {
            if (secondPlayer.isWhite() == player.isWhite()) {
                return (false);
            }
        }

        if (this.firstPlayer == null) {
            this.firstPlayer = player;
        } else if (this.secondPlayer == null) {
            this.secondPlayer = player;
        } else {
            return (false);
        }
        player.init(this.board);
        player.setUndo(this.actionUndo);
        return (true);
    }

    /**
     * Vraci aktualniho hrace
     * 
     * @return Player aktualni hrac
     */
    public Player currentPlayer()
    {
        if (this.currentPlayer) {
            return (this.firstPlayer);
        } else {
            return (this.secondPlayer);
        }
    }

    /**
     * Zmeni aktualniho hrace
     */
    public Player nextPlayer()
    {
        this.currentPlayer = (this.currentPlayer == false);
        return (this.currentPlayer());
    }

    /**
     * Vraci hraci desku
     * 
     * @return Board Hraci deska
     */
    public Board getBoard()
    {
        return (this.board);
    }
    
    /**
     * Provede akci UNDO()
     * 
     * @return boolean Uspech operace
     */
    public boolean undo()
    {
        boolean result = this.actionUndo.removeLastMove();
        if (result == true) {
            this.currentPlayer().addCountDisk();
            this.nextPlayer();
        }
        return (result);
    }

}
