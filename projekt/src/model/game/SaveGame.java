package model.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.board.Board;

public class SaveGame {

	private int maxSaves = 5;
	private int numSaves = 0;
	private Save[] save;

	public SaveGame(String content) {
		this.save = new Save[maxSaves];
		parseSave(content);
	}

	private void parseSave(String save) {
		String[] saves = save.split(";;");
		for (int numSave = 0; numSave < saves.length; numSave++) {
			if (saves[numSave].length() > 1) {
				this.save[numSave] = new Save(saves[numSave]);
				numSaves++;
			}
		}
	}
	
	/**
	 * Získá počet uložených her
	 * 
	 * @return Počet uložených her
	 */
	public int getCountSaves() {
		return (numSaves + 1);
	}
	
	/**
	 * Získá maximální počet uložených her
	 * 
	 * @return Počet maximálních uložených her
	 */
	public int getMaxCountSaves() {
		return (maxSaves);
	}
	
	/**
	 * Získá jméno uložené hry
	 * 
	 * @param numOfSave Číslo uložené hry
	 * @return Název
	 */
	public String getNameOfSave(int numOfSave) {
		if ((numOfSave < numSaves) && (save[numOfSave] != null)) return ((save[numOfSave].name.length() < 1)? "EMPTY NAME" : save[numOfSave].name);
		else return (null);
	}
	
	/**
	 * Získá velikost uložené hry
	 * 
	 * @param numOfSave Číslo uložené hry
	 * @return Velikost
	 */
	public int getSizeOfSave(int numOfSave) {
		if ((numOfSave < numSaves) && (save[numOfSave] != null)) return (save[numOfSave].size);
		else return (8);
	}
	
	/**
	 * Odebere uloženou hru
	 * 
	 * @param numOfSave Číslo uložené hry
	 */
	public void destroySave(int numOfSave) {
		if ((numOfSave < numSaves) && (save[numOfSave] != null)) save[numOfSave] = null;
	}
	
	/**
	 * Získá mód uložené hry
	 * 
	 * @param numOfSave Číslo uložené hry
	 * @return Mód
	 */
	public int getModeOfSave(int numOfSave) {
		if ((numOfSave < numSaves) && (save[numOfSave] != null)) return (save[numOfSave].mode);
		else return (0);
	}
	
	/**
	 * Získá mód uložené hry
	 * 
	 * @param numOfSave Číslo uložené hry
	 * @return Mód
	 */
	public ArrayList<ArrayList<Integer>> getBoardSave(int numOfSave) {
		if ((numOfSave < numSaves) && (save[numOfSave] != null)) return (save[numOfSave].board);
		else return (new ArrayList<ArrayList<Integer>>());
	}
	
	/**
	 * Získá obtížnost uložené hry
	 * 
	 * @param numOfSave Číslo uložené hry
	 * @return Obtížnost
	 */
	public int getLevelOfSave(int numOfSave) {
		if ((numOfSave < numSaves) && (save[numOfSave] != null)) return (save[numOfSave].level);
		else return (0);
	}
	
	/**
	 * Získá zamrzání kamenů uložené hry
	 * 
	 * @param numOfSave Číslo uložené hry
	 * @return Zmarzání
	 */
	public int getOnFreezeOfSave(int numOfSave) {
		if ((numOfSave < numSaves) && (save[numOfSave] != null)) return (save[numOfSave].onFreeze);
		else return (0);
	}
	
	/**
	 * Získá číslo hráče, který je na řadě v uložené hře
	 * 
	 * @param numOfSave Číslo uložené hry
	 * @return Číslo hráče
	 */
	public int getOnMoveOfSave(int numOfSave) {
		if ((numOfSave < numSaves) && (save[numOfSave] != null)) return (save[numOfSave].onMove);
		else return (8);
	}
	
	/**
	 * Zjistí zda je plné uložiště
	 * 
	 * @return Výsledek
	 */
	public boolean isFull() {
		return (maxSaves < numSaves + 1);
	}
	
	/**
	 * Přidá data pro uložení
	 * 
	 * @param name Jeméno savu
	 * @param mode Mód hry
	 * @param level Obtížnost hry
	 * @param onMove Kdo je na tahu
	 * @param onFreeze Zapnutí zamrzání kamenů
	 * @param board Hrací deska
	 * @return Úspěch uložení
	 */
	public boolean addNewSave(String name, int mode, int level, int onMove, int onFreeze, Board board) {
		if (this.maxSaves > numSaves) {
			Save temp = new Save("");
			temp.setName(name);
			temp.boardToSave(board);
			temp.setMode(mode);
			temp.setOnFreeze(onFreeze);
			temp.setOnMove(onMove);
			this.save[numSaves] = temp;
			numSaves++;
			return (true);
		} else {
			System.err.println("Pamět je plná");
			return (false);
		}
	}

	/**
	 * Získá obsah souboru pro uložení
	 * 
	 * @return Obsah
	 */
	public List<String> getDataToSave() {
		String result = "";
		for (int i = 0; i < numSaves; i++) {
			if (save[i] != null)
				result += save[i].getStringRepresentation() + "\n";
		}
		return (Arrays.asList(result));
	}
	
	class Save {
		private String name;
		private int mode = 1;
		private int size = 8;
		private int level = 0;
		private int onMove = 0;
		private int onFreeze = 0;
		private ArrayList<ArrayList<Integer>> board;

		public Save(String save) {
			if (save.length() <= 10)
				return;
			parseSave(save);

		}
		
		/**
		 * Nastaví jméno uložené hry
		 * 
		 * @param name
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * Vrátí velikost hrací desky
		 * 
		 * @return int Velikost
		 */
		public int getSize() {
			return (size);
		}

		/**
		 * Vrátí mód hry
		 * 
		 * @return int Mód
		 */
		public int getMode() {
			return (mode);
		}

		/**
		 * Nastaví mód hry
		 * 
		 * @param mode
		 *            Mód hry
		 */
		public void setMode(int mode) {
			this.mode = mode;
		}
		
		/**
		 * Vrátí číslo hráče, který je na tahu
		 * 
		 * @return int Číslo hráče
		 */
		public int getOnMove() {
			return (onMove);
		}

		/**
		 * Nastaví číslo hráče, který je na tahu
		 * 
		 * @param onMove
		 */
		public void setOnMove(int onMove) {
			this.onMove = onMove;
		}

		/**
		 * Vrátí zamrzání kamenů
		 * 
		 * @return int On/Off
		 */
		public int getOnFreeze() {
			return (onFreeze);
		}

		/**
		 * Nastaví zamrzání kamenů
		 * 
		 * @param onFreeze
		 */
		public void setOnFreeze(int onFreeze) {
			this.onFreeze = onFreeze;
		}

		/**
		 * Vrací úroveň obtížnosti
		 * 
		 * @return int Obtížnost
		 */
		public int getLevel() {
			return (level);
		}

		private void parseSave(String content) {
			String[] data = content.split(";");
			for (int col = 0; col < data.length; col++) {
				
				String[] parsed = data[col].split(":");
				switch (parsed[0]) {
				case "NAME":
					this.name = parsed[1];
					break;
				case "MODE":
					this.mode = Integer.parseInt(parsed[1]);
					break;
				case "ON_MOVE":
					this.onMove = Integer.parseInt(parsed[1]);
					break;
				case "ON_FREEZE":
					this.onFreeze = Integer.parseInt(parsed[1]);
					break;
				case "LEVEL":
					this.level = Integer.parseInt(parsed[1]);
					break;
				case "BOARD":
					String[] rows = parsed[1].replace("[", "").replace(" ", "")
							.split("],");
					this.board = new ArrayList<ArrayList<Integer>>();
					for (int i = 0; i < rows.length; i++) {
						String[] parsedRow = rows[i].replace("]", "")
								.split(",");
						ArrayList<Integer> newRow = new ArrayList<Integer>();
						for (int j = 0; j < parsedRow.length; j++) {
							newRow.add(Integer.parseInt(parsedRow[j]));
						}
						this.board.add(newRow);
					}
					this.size = board.size();
					break;
				default:
					System.err.println("Neznama struktura v souboru.");
					break;
				}
			}
		}

		/**
		 * Projde hrací desku
		 * 
		 * @param board
		 *            Deska
		 */
		public void boardToSave(Board board) {
			this.size = board.getSize();
			this.board = new ArrayList<ArrayList<Integer>>();
			ArrayList<Integer> arrRow;
			int value;
			for (int row = 1; row <= size; row++) {
				arrRow = new ArrayList<Integer>();
				for (int col = 1; col <= size; col++) {
					if (board.getField(row, col).getDisk() == null)
						value = -1;
					else if (board.getField(row, col).getDisk().isWhite())
						value = 0;
					else
						value = 1;
					arrRow.add(value);
				}
				this.board.add(arrRow);
			}
		}

		public String getStringRepresentation() {
			return ("NAME:" + name + ";MODE:" + mode + ";LEVEL:" + level
					+ ";ON_MOVE:" + onMove + ";ON_FREEZE:" + onFreeze
					+ ";BOARD:" + board + ";;");
		}

		@Override
		public String toString() {
			return (getStringRepresentation());
		}
	}
}
