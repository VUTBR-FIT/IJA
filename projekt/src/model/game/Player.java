package model.game;

import model.board.*;
import model.board.Field.Direction;

/**
 * Třída reprezuntující hráče
 * 
 * @author Lukáš Černý (xcerny63)
 * @author Adriana Blašková (xblask02)
 * 
 */
public class Player {

	private final boolean isWhite;
	private int countOfDisk = 0;
	private Undo undo = null;

	public Player(boolean isWhite) {
		this.isWhite = isWhite;
	}

	/**
	 * Nstaví se akce UNDO
	 * 
	 * @param undo Objekt operace Undo
	 */
	public void setUndo(Undo undo) {
		this.undo = undo;
	}

	/**
	 * Otestuje, zda daný hráč může vložit kámen na zadné pole
	 * 
	 * @param field Testované pole
	 * @return boolean Úspěch operace
	 */
	public boolean canPutDisk(Field field) {
		return (field.canPutDisk(new Disk(this.isWhite)));
	}

	/**
	 * Otestuje, zda daný hráč má dostupné kameny
	 * 
	 * @return boolean
	 */
	public boolean emptyPool() {
		return (this.countOfDisk == 0);
	}

	/**
	 * Inicializuje hrací deku
	 * 
	 * @param board Hrací deska
	 */
	public void init(Board board) {
		this.countOfDisk -= 2;
		this.countOfDisk = board.getNumberDisks();
		int center = board.getSize() / 2;
		if (this.isWhite) {
			board.getField(center, center).putDisk(new Disk(true));
			board.getField(center + 1, center + 1).putDisk(new Disk(true));
		} else {
			board.getField(center + 1, center).putDisk(new Disk(false));
			board.getField(center, center + 1).putDisk(new Disk(false));
		}
	}

	/**
	 * Otestuje, zda je daný hráč bílý
	 * 
	 * @return boolean
	 */
	public boolean isWhite() {
		return (this.isWhite);
	}

	/**
	 * Pokud je to možné, tak položí kámen na dané pole
	 * 
	 * @param field Pole kam se vloží kámen
	 * @return boolean Úspěch operace
	 */
	public boolean putDisk(Field field) {
		boolean canPut = false;
		if (field.getDisk() == null) {
			boolean temp = false;
			for (Direction c : Direction.values()) {
				temp = this.putDiskDirs(c, field.nextField(c), 1);
				canPut = canPut || temp;
			}
			if (canPut) {
				field.putDisk(new Disk(isWhite));
				this.undo.endMove(field);
				this.countOfDisk--;
			}
		}
		return (canPut);
	}

	/**
	 * Projde hrací desku v daném směru a pokud může otočit kámen tak otočí
	 * 
	 * @param dirs Směr pohybu
	 * @param field Aktuální pole
	 * @param length Vzdálenost od původního kamene
	 * @return boolean
	 */
	private boolean putDiskDirs(Field.Direction dirs, Field field, int length) {
		boolean canPut = false;
		if (field.getDisk() != null) {
			if ((length == 1) && (field.getDisk().isWhite() != this.isWhite)) {
				canPut = this.putDiskDirs(dirs, field.nextField(dirs),
						length + 1);
			} else if (length > 1) {
				if (field.getDisk().isWhite() == this.isWhite) {
					return (true);
				}
				canPut = this.putDiskDirs(dirs, field.nextField(dirs),
						length + 1);
			}
		}
		if (canPut) {
			field.getDisk().turn();
			this.undo.addTurnDisk(field);
		}
		return (canPut);
	}

	/**
	 * Zvýší počet dostupných disků
	 */
	public void addCountDisk() {
		this.countOfDisk++;
	}
	
	/**
	 * Sníží počdt dostupných disků
	 */
	public void removeCountDisk() {
		this.countOfDisk--;
	}

	@Override
	public String toString() {
		return ((this.isWhite) ? "bílý" : "černý");
	}
}
