package model.game;

import java.util.ArrayList;

import model.board.Field;

/**
 * Třída reprezuntující tahy za hru
 * 
 * @author Lukáš Černý (xcerny63)
 * @author Adriana Blašková (xblask02)
 * 
 */
public class Undo {

	private final ArrayList<UndoMove> moves;
	private UndoMove activeMoves = null;

	public Undo() {
		this.moves = new ArrayList<UndoMove>();
	}

	/**
	 * Přidá na konec fronty nově položený kámen
	 * 
	 * @param field Pole s nově položeným kamenem
	 */
	public void endMove(Field field) {
		if (this.activeMoves != null) {
			this.activeMoves.addMoves(field);
			this.storeMoves();
		}
	}

	/**
	 * Přidá otočený kámen
	 * 
	 * @param field Pole s otočeným kamenem
	 */
	public void addTurnDisk(Field field) {
		if (this.activeMoves == null)
			this.activeMoves = new UndoMove();
		this.activeMoves.addMoves(field);
	}

	/**
	 * Odstraní poslední tah
	 * 
	 * @return boolean Vrací uspěch operace
	 */
	public boolean removeLastMove() {
		if (this.moves.size() > 0) {
			int position = this.moves.size() - 1;
			UndoMove move = this.moves.get(position);
			move.removeMoves();
			this.moves.remove(position);
			return (true);
		} else {
			return (false);
		}
	}

	/**
	 * Přidá provedený tah do pole tahů
	 */
	private void storeMoves() {
		if (this.activeMoves != null) {
			this.moves.add(this.activeMoves);
			this.activeMoves = null;
		}
	}

	/**
	 * Třída reprezentují tah
	 * 
	 * @author Lukáš Černý (xcerny63)
	 * @author Adriana Blašková (xblask02)
	 * 
	 */
	class UndoMove {

		ArrayList<Field> move;

		UndoMove() {
			move = new ArrayList<Field>();
		}

		/**
		 * Přidá pole s otočeným nebo přidaným diskem na konec seznamu
		 * 
		 * @param field Přidávané pole
		 */
		void addMoves(Field field) {
			move.add(field);
		}

		/**
		 * Vrací změny zpět
		 */
		void removeMoves() {
			BoardField field;
			for (int i = 0; i < move.size(); i++) {
				field = (BoardField) move.get(i);
				field.getDisk().turn();
			}
			field = (BoardField) move.get(move.size() - 1);
			field.removeDisk();
		}
	}
};
