package model.game;

import model.board.AbstractBoardField;

/**
 * Třída reprezuntující hrací pole
 * 
 * @author Lukáš Černý (xcerny63)
 * @author Adriana Blašková (xblask02)
 * 
 */
public class BoardField extends AbstractBoardField
{
    public BoardField(int row, int col) {
		super(row, col);
	}
}
