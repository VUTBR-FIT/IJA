/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.game;

import model.board.Field;
import model.board.Rules;

/**
 * Třída reprezuntující tahy za hru
 * 
 * @author Lukáš Černý (xcerny63)
 * @author Adriana Blašková (xblask02)
 * 
 */
public class ReversiRules implements Rules
{

    private final int size;

    public ReversiRules(int size)
    {
        this.size = size;
    }

    @Override
    public Field createField(int row, int col)
    {
        return (new BoardField(row, col));
    }

    @Override
    public int getSize()
    {
        return (this.size);
    }

    @Override
    public int numberDisks()
    {
        return (this.size * this.size);
    }

}
