package model.board;

/**
 *
 * @author Lukas
 */
public class Board extends java.lang.Object
{

    private Field[][] array;
    private Rules rules;

    public Board(Rules rules)
    {
        this.rules = rules;
        this.array = new Field[rules.getSize() + 2][rules.getSize() + 2];
        this.generateField();
        this.setNeighborFields();
    }

    /**
     * Získá pole, které se vyskytuje na daném umístění
     * 
     * @param row Řádek
     * @param col Sloupec
     * @return Field
     */
    public Field getField(int row, int col)
    {
        if ((row >= 0) && (row <= rules.getSize() + 1) && (col >= 0) && (col <= rules.getSize() + 1)) {
            return (array[row][col]);
        } else {
            return (null);
        }
    }

    /**
     * Vrací velikost hrací desky
     * 
     * @return int Velikost desky
     */
    public int getSize()
    {
        return (rules.getSize());
    }

    /**
     * Vrací počet disků
     * 
     * @return int Počet disků	
     */
    public int getNumberDisks()
    {
        return (this.rules.numberDisks());
    }

    private void generateField()
    {
        int size = rules.getSize();
        Field border = new BorderField();
        for (int row = 0; row <= size + 1; row++) {
            for (int col = 0; col <= size + 1; col++) {
                if ((row == 0) || (row == size + 1)) {
                    this.array[row][col] = border;
                } else if ((col == 0) || (col == size + 1)) {
                    this.array[row][col] = border;
                } else {
                    this.array[row][col] = rules.createField(row, col);
                }
            }
        }
    }

    private void setNeighborFields()
    {
        for (int row = 1; row <= rules.getSize(); row++) {
            for (int col = 1; col <= rules.getSize(); col++)
            {
                array[row][col].addNextField(Field.Direction.D, this.array[row + 1][col]);
                array[row][col].addNextField(Field.Direction.L, this.array[row][col - 1]);
                array[row][col].addNextField(Field.Direction.LD, this.array[row + 1][col - 1]);
                array[row][col].addNextField(Field.Direction.LU, this.array[row - 1][col - 1]);
                array[row][col].addNextField(Field.Direction.R, this.array[row][col + 1]);
                array[row][col].addNextField(Field.Direction.RD, this.array[row + 1][col + 1]);
                array[row][col].addNextField(Field.Direction.RU, this.array[row - 1][col + 1]);
                array[row][col].addNextField(Field.Direction.U, this.array[row - 1][col]);
            }
        }
    }
}
