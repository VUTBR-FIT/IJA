package ija.ija2015.homework2.board;

public class AbstractBoardField extends java.lang.Object implements Field
{

    private int row;
    private int col;

    private Disk disk = null;

    private Field[] around;

    public AbstractBoardField(int row, int col)
    {
        this.col = col;
        this.row = row;
        this.around = new Field[8];
    }

    @Override
    public void addNextField(Field.Direction dirs, Field field)
    {
        int key = 0;
        for (Direction c : Direction.values()) {
            if (dirs == c) {
                break;
            }
            key++;
        }
        this.around[key] = field;
    }

    @Override
    public Disk getDisk()
    {
        return (this.disk);
    }

    @Override
    public Field nextField(Field.Direction dirs)
    {
        int key = 0;
        for (Direction c : Direction.values()) {
            if (dirs == c) {
                break;
            }
            key++;
        }
        return (this.around[key]);
    }

    @Override
    public boolean putDisk(Disk disk)
    {
        if (this.disk != null) {
            return (false);
        } else {
            this.disk = disk;
            return (true);
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if ((obj != null) && (obj instanceof AbstractBoardField) && (this.getClass() == obj.getClass())) {
            final AbstractBoardField other = (AbstractBoardField) obj;
            if ((other.row == this.row) && (other.col == this.col)) {
                return (true);
            }
        }
        return (false);
    }

    @Override
    public int hashCode()
    {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    /*
    public String toString()
    {
        return ("Pole(" + row + "," + col + ") - Kamen:" + disk);
    }
     */

    @Override
    public boolean canPutDisk(Disk disk) {
        return ((this.disk != null)? false : true);
    }
}
