package ija.ija2015.homework2.game;

import ija.ija2015.homework2.board.Field;
import java.util.ArrayList;

public class Undo
{

    private final ArrayList<UndoMoves> moves;
    private UndoMoves activeMoves = null;

    public Undo()
    {
        this.moves = new ArrayList<UndoMoves>();
    }

    public void startMove(Field field)
    {
        if (this.activeMoves != null) {
            this.storeMoves();
        } else {
            this.activeMoves = new UndoMoves();
        }
        this.activeMoves.addMoves(field);
    }

    public boolean removeLastMove()
    {
        if (this.moves.size() == 0) {
            return (false);
        }
        int position = this.moves.size();
        this.moves.get(position).removeMoves();
        this.moves.remove(position);
        return (true);
    }

    private void storeMoves()
    {
        this.moves.add(this.activeMoves);
        this.activeMoves = new UndoMoves();
    }

    class UndoMoves
    {

        ArrayList<Field> moves;

        void create()
        {
            moves = new ArrayList<Field>();
        }

        void addMoves(Field field)
        {
            moves.add(field);
        }

        void removeMoves()
        {
            //BoardField field1 = (BoardField) moves.get(0);
            //field1.removeDisk();
            for (int i = 1; i < moves.size(); i++) {
                moves.get(i).getDisk().turn();
            }
        }
    }
};
