/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.ija2015.homework1.board;

/**
 *
 * @author Lukas
 */
public class BoardField extends java.lang.Object implements Field
{//OK

    private int row;
    private int col;

    private boolean definedDisk;
    private Disk disk;

    private Field[] around;

    public BoardField(int row, int col)
    {
        this.col = col;
        this.row = row;
        this.definedDisk = false;
        this.disk = null;
        this.around = new Field[8];
    }

    @Override
    public void addNextField(Field.Direction dirs, Field field)
    {
        int key = 0;
        for (Direction c : Direction.values()) {
            if (dirs == c) {
                break;
            }
            key++;
        }
        this.around[key] = field;
    }

    @Override
    public Disk getDisk()
    {
        return (this.disk);
    }

    @Override
    public Field nextField(Field.Direction dirs)
    {
        int key = 0;
        for (Direction c : Direction.values()) {
            if (dirs == c) {
                break;
            }
            key++;
        }
        return (this.around[key]);
    }

    @Override
    public boolean putDisk(Disk disk)
    {
        if (this.definedDisk) {
            return (false);
        } else {
            this.definedDisk = true;
            this.disk = disk;
            return (true);
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if ((obj != null) && (obj instanceof BoardField) && (this.getClass() == obj.getClass())) {
            final BoardField other = (BoardField) obj;
            if ((other.row == this.row) && (other.col == this.col)) {
                return (true);
            }
        }
        return (false);
    }

    @Override
    public int hashCode()
    {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

}
