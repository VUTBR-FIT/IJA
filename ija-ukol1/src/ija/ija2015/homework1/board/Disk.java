package ija.ija2015.homework1.board;

/**
 *
 * @author Lukas
 */
public class Disk extends java.lang.Object
{//OK

    private boolean isWhiteColor;

    public Disk(boolean isWhite)
    {
        this.isWhiteColor = isWhite;
    }

    public boolean isWhite()
    {
        return (this.isWhiteColor);
    }

    public void turn()
    {
        if (this.isWhiteColor == true) {
            this.isWhiteColor = false;
        } else {
            this.isWhiteColor = true;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if ((obj != null) && (obj instanceof Disk) && (this.getClass() == obj.getClass())) {
            final Disk other = (Disk) obj;
            if (other.isWhiteColor == this.isWhiteColor) {
                return (true);
            }
        }
        return (false);
    }

    @Override
    public int hashCode()
    {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

}
