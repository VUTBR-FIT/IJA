package ija.ija2015.homework1.board;

/**
 *
 * @author Lukas
 */
public class BorderField extends java.lang.Object implements Field
{

    public BorderField()
    {

    }

    @Override
    public void addNextField(Direction dirs, Field field)
    {
    }

    @Override
    public Disk getDisk()
    {
        return (null);
    }

    @Override
    public Field nextField(Direction dirs)
    {
        return (null);
    }

    @Override
    public boolean putDisk(Disk disk)
    {
        return (false);
    }

}
