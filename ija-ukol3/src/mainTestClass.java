

import org.junit.Test;

import project.board.Board;
import project.board.Field;
import project.board.Rules;
import project.game.Game;
import project.game.Player;
import project.game.ReversiRules;
import static org.junit.Assert.*;

/**
 * @author Lukas Cerny (xcerny63)
 * @author Adriana Blaskova (xblask02)
 */
public class mainTestClass
{

    @Test
    public void testPlayer()
    {
        System.out.println("Player");

        Player p1 = new Player(true);
        assertTrue("Test prazdne sady kamenu.", p1.emptyPool());
        assertEquals("Test spravne textove reprezentace objektu.", "hrac:white", "hrac:" + p1);

        Player p2 = new Player(false);
        assertTrue("Test prazdne sady kamenu.", p2.emptyPool());
        assertEquals("Test spravne textove reprezentace objektu.", "hrac:black", "hrac:" + p2);
    }

    /**
     * Test pravidel inicializace.
     */
    @Test
    public void testRules()
    {
        System.out.println("Rules");
        int size = 8;

        Rules rules = new ReversiRules(size);
        assertEquals("Test velikosti hry", size, rules.getSize());
        assertEquals("Test poctu kamenu pro jednoho hrace", size * size / 2, rules.numberDisks());

        Field f1 = rules.createField(2, 3);
        Field f2 = rules.createField(2, 3);
        Field f3 = rules.createField(4, 4);

        assertEquals("Test shody dvou stejnych poli.", f1, f2);
    }

    /**
     * Test hry (vytvoreni pravidel, desky a hracu, tahu a undo).
     */
    @Test
    public void testGame()
    {
        System.out.println("Game");
        int size = 8;

        ReversiRules rules = new ReversiRules(size);
        Board board = new Board(rules);
        Game game = new Game(board);

        Player p1 = new Player(true);
        Player p2 = new Player(false);
        Player p3 = new Player(true);

        game.addPlayer(p1);
        game.addPlayer(p2);
        assertFalse("Test pridani tretiho hrace.", game.addPlayer(p3));
        assertFalse("Test neprazdne sady kamenu.", p1.emptyPool());
        assertFalse("Test neprazdne sady kamenu.", p2.emptyPool());

        assertEquals("Test, zda je aktualni hrac bily.", p1, game.currentPlayer());

        assertTrue("Test spravneho umisteni pocatecnich kamenu.", game.getBoard().getField(4, 4).getDisk().isWhite());
        assertTrue("Test spravneho umisteni pocatecnich kamenu.", game.getBoard().getField(5, 5).getDisk().isWhite());
        assertFalse("Test spravneho umisteni pocatecnich kamenu.", game.getBoard().getField(4, 5).getDisk().isWhite());
        assertFalse("Test spravneho umisteni pocatecnich kamenu.", game.getBoard().getField(5, 4).getDisk().isWhite());

        Field f1 = game.getBoard().getField(3, 4);
        Field f2 = game.getBoard().getField(4, 6);
        assertFalse("Test umisteni kamene na spatnou pozici.", game.currentPlayer().canPutDisk(f1));
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f2));
        assertTrue("Umisteni kamene.", game.currentPlayer().putDisk(f2));

        // System.out.println(game.getBoard());

        game.nextPlayer();
        assertEquals("Test, zda je aktualni hrac cerny.", p2, game.currentPlayer());

        f2 = game.getBoard().getField(5, 6);
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f2));
        assertTrue("Umisteni kamene.", game.currentPlayer().putDisk(f2));

        for (int i = 4; i <= 6; i++)
        {
            assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, i).getDisk().isWhite());
            assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(5, i).getDisk().isWhite());
        }

        game.nextPlayer();
        assertEquals("Test, zda je aktualni hrac bily.", p1, game.currentPlayer());

        f2 = game.getBoard().getField(6, 4);
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f2));
        assertTrue("Umisteni kamene.", game.currentPlayer().putDisk(f2));

        game.nextPlayer();
        assertEquals("Test, zda je aktualni hrac cerny.", p2, game.currentPlayer());

        f2 = game.getBoard().getField(3, 6);
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f2));
        assertTrue("Umisteni kamene.", game.currentPlayer().putDisk(f2));

        game.nextPlayer();
        assertEquals("Test, zda je aktualni hrac bily.", p1, game.currentPlayer());

        f2 = game.getBoard().getField(3, 7);
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f2));
        assertTrue("Umisteni kamene.", game.currentPlayer().putDisk(f2));
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 4).getDisk().isWhite());
        game.nextPlayer();
        assertEquals("Test, zda je aktualni hrac cerny.", p2, game.currentPlayer());

        f2 = game.getBoard().getField(3, 8);
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f2));
        assertTrue("Umisteni kamene.", game.currentPlayer().putDisk(f2));

        game.nextPlayer();
        assertEquals("Test, zda je aktualni hrac bily.", p1, game.currentPlayer());

        f2 = game.getBoard().getField(2, 8);
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f2));
        assertTrue("Umisteni kamene.", game.currentPlayer().putDisk(f2));

        game.nextPlayer();
        assertEquals("Test, zda je aktualni hrac cerny.", p2, game.currentPlayer());

        f2 = game.getBoard().getField(1, 8);
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f2));
        assertTrue("Umisteni kamene.", game.currentPlayer().putDisk(f2));

        game.nextPlayer();
        assertEquals("Test, zda je aktualni hrac bily.", p1, game.currentPlayer());

        f2 = game.getBoard().getField(3, 9);
        assertFalse("Test umisteni kamene na spatnou pozici.", game.currentPlayer().canPutDisk(f2));

        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(1, 8).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(2, 8).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(3, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(3, 7).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(3, 8).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 5).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 5).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(5, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(6, 4).getDisk().isWhite());

    }

    @Test
    public void testUndo()
    {
        System.out.println("Undo");
        int size = 8;

        ReversiRules rules = new ReversiRules(size);
        Board board = new Board(rules);
        Game game = new Game(board);

        Player p1 = new Player(true);
        Player p2 = new Player(false);

        game.addPlayer(p1);
        game.addPlayer(p2);

        Field f1 = game.getBoard().getField(4, 6);
        game.currentPlayer().putDisk(f1);

        game.nextPlayer();
        f1 = game.getBoard().getField(5, 6);
        game.currentPlayer().putDisk(f1);

        game.nextPlayer();
        f1 = game.getBoard().getField(6, 4);
        game.currentPlayer().putDisk(f1);

        game.nextPlayer();
        f1 = game.getBoard().getField(3, 6);
        game.currentPlayer().putDisk(f1);

        game.nextPlayer();
        f1 = game.getBoard().getField(3, 7);
        game.currentPlayer().putDisk(f1);

        game.nextPlayer();
        f1 = game.getBoard().getField(3, 8);
        game.currentPlayer().putDisk(f1);

        game.nextPlayer();
        f1 = game.getBoard().getField(2, 8);
        game.currentPlayer().putDisk(f1);

        game.nextPlayer();
        f1 = game.getBoard().getField(1, 8);
        game.currentPlayer().putDisk(f1);

        game.nextPlayer();

        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(1, 8).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(2, 8).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(3, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(3, 7).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(3, 8).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 5).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 5).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(5, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(6, 4).getDisk().isWhite());

        assertTrue("Test undo.", game.undo());
        assertEquals("Test, zda je aktualni hrac cerny.", p2, game.currentPlayer());
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f1));
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(2, 8).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(3, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(3, 7).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(3, 8).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 5).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 5).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(5, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(6, 4).getDisk().isWhite());

        assertTrue("Test undo.", game.undo());
        f1 = game.getBoard().getField(2, 8);
        assertEquals("Test, zda je aktualni hrac bily.", p1, game.currentPlayer());
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f1));
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(3, 6).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(3, 7).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(3, 8).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 5).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 5).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(5, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(6, 4).getDisk().isWhite());

        assertTrue("Test undo.", game.undo());
        f1 = game.getBoard().getField(3, 8);
        assertEquals("Test, zda je aktualni hrac cerny.", p2, game.currentPlayer());
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f1));
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(3, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(3, 7).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 5).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 5).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(5, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(6, 4).getDisk().isWhite());

        assertTrue("Test undo.", game.undo());
        f1 = game.getBoard().getField(3, 7);
        assertEquals("Test, zda je aktualni hrac bily.", p1, game.currentPlayer());
        assertTrue("Test umisteni kamene na dobrou pozici.", game.currentPlayer().canPutDisk(f1));
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(3, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(4, 5).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(4, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 4).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(5, 5).getDisk().isWhite());
        assertFalse("Test spravne barvy kamene (cerny).", game.getBoard().getField(5, 6).getDisk().isWhite());
        assertTrue("Test spravne barvy kamene (bily).", game.getBoard().getField(6, 4).getDisk().isWhite());

    }

}
