package project.board;

/**
 *
 * @author Lukas
 */
public class BorderField extends java.lang.Object implements Field
{

    public BorderField() {}

    @Override
    public void addNextField(Direction dirs, Field field)
    {
    }

    @Override
    public Disk getDisk()
    {
        return (null);
    }

    @Override
    public Field nextField(Direction dirs)
    {
        return (null);
    }

    @Override
    public boolean putDisk(Disk disk)
    {
        return (false);
    }

    @Override
    public boolean canPutDisk(Disk disk)
    {
        return (false);
    }
    
    public String toString()
    {
        return ("Prazdne pole");
    }
}
