package project.board;

/**
 *
 * @author Lukas
 */
public interface Field
{

    public enum Direction
    {
        D,
        L,
        LD,
        LU,
        R,
        RD,
        RU,
        U
    }

    public void addNextField(Field.Direction dirs, Field field);

    public Disk getDisk();

    public Field nextField(Field.Direction dirs);

    public boolean putDisk(Disk disk);

    public boolean canPutDisk(Disk disk);
    
    public String toString();
}
