package project.game;

import project.board.AbstractBoardField;

/**
 *
 * @author Lukas
 */
public class BoardField extends AbstractBoardField
{
    public BoardField(int row, int col) {
		super(row, col);
	}
}
