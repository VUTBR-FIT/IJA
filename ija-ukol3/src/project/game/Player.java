package project.game;

import project.board.Board;
import project.board.Disk;
import project.board.Field;
import project.board.Field.Direction;

/**
 *
 * @author Lukas
 */
public class Player extends java.lang.Object
{

    private final boolean isWhite;
    private int countOfDisk = 0;
    private Undo undo = null;

    public Player(boolean isWhite)
    {
        this.isWhite = isWhite;
    }

    public void setUndo(Undo undo)
    {
        this.undo = undo;
    }
    public boolean canPutDisk(Field field)
    {
        return (field.canPutDisk(new Disk(this.isWhite)));
    }

    public boolean emptyPool()
    {
        return (this.countOfDisk == 0);
    }

    public void init(Board board)
    {
        this.countOfDisk -= 2;
        this.countOfDisk = board.getNumberDisks();
        int center = board.getSize() / 2;
        if (this.isWhite) {
            board.getField(center, center).putDisk(new Disk(true));
            board.getField(center + 1, center + 1).putDisk(new Disk(true));
        } else {
            board.getField(center + 1, center).putDisk(new Disk(false));
            board.getField(center, center + 1).putDisk(new Disk(false));
        }
    }

    public boolean isWhite()
    {
        return (this.isWhite);
    }
    
    public boolean putDisk(Field field)
    {
        boolean canPut = false;
        if (field.getDisk() == null) {
            boolean temp = false;
            for (Direction c : Direction.values()) {
                temp = this.putDiskDirs(c, field.nextField(c), 1);
                canPut = canPut || temp;
            }
            if (canPut) {
                field.putDisk(new Disk(isWhite));
                this.undo.endMove(field);
                this.countOfDisk--;
            }
        }
        return (canPut);
    }

    private boolean putDiskDirs(Field.Direction dirs, Field field, int length)
    {
        boolean canPut = false;
        if (field.getDisk() != null) {
            if ((length == 1) && (field.getDisk().isWhite() != this.isWhite)) {
                canPut = this.putDiskDirs(dirs, field.nextField(dirs), length + 1);
            } else if (length > 1) {
                if (field.getDisk().isWhite() == this.isWhite) {
                    return (true);
                }
                canPut = this.putDiskDirs(dirs, field.nextField(dirs), length + 1);
            }
        }
        if (canPut) {
            field.getDisk().turn();
            this.undo.addTurnDisk(field);
        }
        return (canPut);
    }
    
    public void addCountDisk() {
        this.countOfDisk++;
    }

    @Override
    public String toString()
    {
        return ((this.isWhite) ? "white" : "black");
    }
}
