package project.game;

import java.util.ArrayList;

import project.board.Field;


public class Undo
{

    private final ArrayList<UndoMove> moves;
    private UndoMove activeMoves = null;

    public Undo()
    {
        this.moves = new ArrayList<UndoMove>();
    }

    public void endMove(Field field)
    {
        if (this.activeMoves != null) {
            this.activeMoves.addMoves(field);
            this.storeMoves();
        }
    }
    
    public void addTurnDisk(Field field)
    {
        if (this.activeMoves != null) this.storeMoves();
        this.activeMoves = new UndoMove();
        this.activeMoves.addMoves(field);
    }

    public boolean removeLastMove()
    {
        if (this.moves.size() > 0) {
            int position = this.moves.size() - 1;
            UndoMove move = this.moves.get(position);
            move.removeMoves();
            this.moves.remove(position);
            return (true);
        } else {
            return (false);
        }
    }

    private void storeMoves()
    {
        if (this.activeMoves != null) {
            this.moves.add(this.activeMoves);
            this.activeMoves = null;
        }
    }

    class UndoMove
    {

        ArrayList<Field> moves;

        UndoMove()
        {
            moves = new ArrayList<Field>();
        }

        void addMoves(Field field)
        {
            moves.add(field);
        }

        void removeMoves()
        {
            BoardField field1 = (BoardField) moves.get(moves.size()-1);
            field1.removeDisk();
            for (int i = 0; i < moves.size() - 1; i++) {
                moves.get(i).getDisk().turn();
            }
        }
    }
};
